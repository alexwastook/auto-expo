# Documentation Auto-Expo

## Description

Auto-Expo et un petit site web local sans hebergeur (tout opére sur la machine devant vous !)
conçu pour que les étudiant(e)s puissent mettre cote-à-cote les positifs et négatifs de
leur travaux sans passer par un service tierce

## Usage

### Pre-remplissage

chaque élève envoie un dossier contenant ses "paires"
une paire est constituée de deux fichiers, ces fichiers
sont des images au format png qui doivent étre nomées
l'une "positif-x" et l'autre "negatif-x" avec x le numéro
de la paire, normalement avec un élève appelé "example"
ayant 2 paires l'aboresence devrait resembler à :

```
auto-expo
├── images
│   └── example
│       ├── negatif-1.png
│       ├── negatif-2.png
│       ├── positif-1.png
│       └── positif-2.png
├── index.html
├── index.js
└── README.md
```

Il n'y a pas de limite au nombre de paires d'un éléve.
Une fois reçu, le dossier de l'élève doit étre placer dans
le dossier 'images'

### Show time !

méme si toutes les images des élèves restent bien gentiment
dans la machine locale, l'ordinateur doit étre connecter à
internet pour pouvoir accéder a une "boite a outils"
(sinon il sera super moche)

pour voir le résultat il faut ouvrir le fichier `index.html`
avec votre navigateur favoris, saisir le nom du dossier que l'on
souhaite consulter dans le champ "Identifiant exposition"
(il n'y a que celui là de toute façon) et cliquer sur le
boutton "Découvrir". Tadamm

Cliquer sur le bord d'une image passe du positif au négatif et vice-versa.

Cliquer sur une image l'ouvre dans un onglet a part.

Le site est compatible téléphones.

### Demo  

Une démo est consultable [ici.](https://auto-expo-alexwastook-63f0f6850eb4f0c706d47ab5abdaecea4a3ef9d5d.gitlab.io/) le seul dossier existant est "example"

## Résolution de problémes

*"mon image n'apparait pas !"*  
- Est elle bien au format png ?
- Est elle bien nomée "positif-x" ou "negatif-x" (x est un numéro ex: 1, 2, 3...) ?
- Esque les numéros des paires sont bien continus ?
- Esque elle s'ouvre correctement hors de l'expo ?

*"c'est tout cassé il n'y a plus rien !"*  
Il faut verifier que l'ordinateur est bien connécté a internet.


*"418 I'm a teapot"*  
Le serveur refuse de préparer du café, car il s'agit d'une théière.

## Informations annexes

Les images viennent de https://picsum.photos/ un site fournissant
l'équivalent photos du célébre texte https://fr.lipsum.com/


Un doute ou une question sur une partie du code ?
vous trouverez surement un article d'explication sur
https://developer.mozilla.org/ c'est une référence mondiale
en matiére de dévlopement web.

## License

Auto-Expo vous est fournis sous les termes de la license Beerware
https://fr.wikipedia.org/wiki/Beerware

