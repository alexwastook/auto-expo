const expo = document.querySelector(".expo");
const form = document.forms.uidForm;

function carousel(uid,pid){
	const c = document.createElement('div');
	c.innerHTML =
		`<div id="carousel-${uid}-${pid}" class="carousel slide carousel-fade pb-3">
	      <div class="carousel-inner">
	        <div class="carousel-item active">
	          <img src="./images/${uid}/positif-${pid}.png" class="d-block w-100" onclick="window.open('./images/${uid}/positif-${pid}.png', '_blank');">
	        </div>
	        <div class="carousel-item">
	          <img src="./images/${uid}/negatif-${pid}.png" class="d-block w-100" onclick="window.open('./images/${uid}/negatif-${pid}.png', '_blank');">
	        </div>
	      <button class="carousel-control-prev" type="button" data-bs-target="#carousel-${uid}-${pid}" data-bs-slide="prev">
	        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
	        <span class="visually-hidden">Previous</span>
	      </button>
	      <button class="carousel-control-next" type="button" data-bs-target="#carousel-${uid}-${pid}" data-bs-slide="next">
	        <span class="carousel-control-next-icon" aria-hidden="true"></span>
	        <span class="visually-hidden">Next</span>
	      </button>
	    </div>`;
	return c;
}

function getCarouselsOf(uid, pid=1, carousels=[]){
    var img = new Image();
	img.onload = () => {
		carousels.push(carousel(uid, pid));
		getCarouselsOf(uid, pid+1, carousels)
	}
	img.onerror = () => {
		expo.innerHTML = '';
		carousels.forEach(element => expo.appendChild(element));
	}
	img.src = `./images/${uid}/negatif-${pid}.png`;
    
}

form.addEventListener("submit", (event) => {
    event.preventDefault();
    const data = new FormData(form);
	getCarouselsOf(
		data.get("uid-input")
	);
});
